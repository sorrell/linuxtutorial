# History

> **Prereqs**
> - Usage of `linuxtut` Docker container
> - Usage of the user `newuser` inside container (`su` if needed)

When you log into a new server to diagnose problems, a lot of times you'll wonder what someone had been doing in there before you. What commands did they run? Switch to `newuser` if you aren't already using that user, make sure you're in the home directory of `newuser`, and let's execute the commands below:

```sh
newuser@50f4cbf77e48:~$ echo "Hello bash history!"
newuser@50f4cbf77e48:~$ cat ~/.bash_history
newuser@50f4cbf77e48:~$ exit
root@50f4cbf77e48:/# su newuser
newuser@50f4cbf77e48:/$ cd ~
newuser@50f4cbf77e48:/$ cat ~/.bash_history
echo "Hello bash history!"
cat ~/.bash_history
exit
```

You will notice that the first time we `cat` the .bash_history, we don't see our `echo` command, but upon exiting and switching back, we see the `echo`, the `cat`, and the `exit`. That's because these commands are saved upon exiting the shell. (Note: your results may have more in the history if you've entered more commands).

So what if we want to see the history our commands before we exit? Well, that is where the `history` command comes in.

```sh
newuser@50f4cbf77e48:~$ history
    1  echo "Hello bash history!"
    2  cat ~/.bash_history
    3  exit
    4  cd ~
    5  cat ~/.bash_history
    6  history
```

Sometimes this history can be very long, and that's where the `n` value comes in handy. Below we will view just the past 4 (including the current) commands by specifying an `n` of `4`:

```sh
newuser@50f4cbf77e48:~$ history 4
    4  cd ~
    5  cat ~/.bash_history
    6  history
    7  history 4
```

Again, a lot of times our history will be huge. Remember our usage of pipes and `grep`? Let's try those again to grab just the `cat` commands:

```sh
newuser@50f4cbf77e48:~$ history | grep -i cat
    2  cat ~/.bash_history
    5  cat ~/.bash_history
    8  history | grep -i cat
```

## The "WHEN" of the Command

So far we've seen which commands were executed, but we don't know when they were executed. Let's use the commands below to add times to our history, and we'll also discover "redirection" of commands and "sourcing."

```sh
newuser@50f4cbf77e48:~$ echo 'export HISTTIMEFORMAT="%d/%m/%y %T "' >> ~/.bash_profile
newuser@50f4cbf77e48:~$ source ~/.bash_profile
newuser@50f4cbf77e48:~$ history 4
    8  21/02/20 18:27:07 history | grep -i cat
    9  21/02/20 18:29:28 echo 'export HISTTIMEFORMAT="%d/%m/%y %T "' >> ~/.bash_profile
   10  21/02/20 18:29:35 source ~/.bash_profile
   11  21/02/20 18:29:43 history 4
```

As you can see above, we use the `echo` command to echo a command to `export` the `HISTTIMEFORMAT` variable. We have seen and used the `export` command before, so we know what's going on there, but why are we echoing it?

Well, simply echoing it does nothing on it's own, but the real magic is in the `>>`. We are redirecting the output to a file, `.bash_profile` in this case. `>>` will create the file if it doesn't exist, or append to an existing file if it does. A single `>` would redirect the output to a new file, and error if that file already exists. 

Next, we used the `source` command on our `.bash_profile` file. As we have talked about before, when we start a shell session, the `.bashrc`, `.bash_profile` and `.profile` files are all automatically loaded - the term for loading them is called `sourcing` them. So, with the `source` command, we just load everything it specifies into our current shell session.

Instead of the combination of `echo` and `source` above, we could have just ran this command below, but our history wouldn't have times the next time we log into `newuser`'s account.

`export HISTTIMEFORMAT="%d/%m/%y %T "`

To wrap up this section, it's important to realize that the dotfiles, including the `.bash_history`, is user-specific. That means that the `root` user has a different history (and dotfiles) than the `newuser` user. Let's exit the `newuser` session and look at the `root` user's history (your history will probably look a little different).

```sh
newuser@50f4cbf77e48:~$ exit
exit
root@50f4cbf77e48:/# history
    1  echo $$
    2  echo $MY_VAR
    3  exit
    4  MY_VAR="Hello world! This is my new envvar!"
    5  echo MY_VAR
    6  su newuser
    7  history
```

## Commands & Concepts We Covered
 
  - Redirects
  - TODO