# Dotfiles

> **Prereqs**
> - Usage of `linuxtut` Docker container
> - Knowledge of [environment variables](02-Environment.md)

Normally, you won't be doing anything as the root user unless you are inside of a docker container. So, let's switch to a non-privileged user (root is a privileged user, meaning it can do anything).

For that, we will use the `su` (**s**witch **u**ser) command, and use some of our commands from before to check out our environment.

```sh
root@25ec7419d9e7:/\# su newuser
newuser@25ec7419d9e7:/$ whoami
newuser
newuser@25ec7419d9e7:/$ pwd
/
newuser@25ec7419d9e7:/$ echo $HOME
/home/newuser
newuser@25ec7419d9e7:/$ cd $HOME
newuser@25ec7419d9e7:~$ 
```

Notice above when we switched from root to newuser, we no longer had the `#` symbol. It's now `$` which means we are not the root user. Also, notice that when we `cd` (**c**hange **d**irectory) from the root of the filesystem (`/`) to `$HOME`, we see a new symbol, `~`. This is short for `$HOME`, so the following are equivalent:

`cd $HOME == cd ~`

Since this section is about dotfiles, let's start exploring them! `newuser` is going to have a few dotfiles for us to review, but first we need to find out their names. They are called dotfiles because there is a `.` that prefixes them, and that mean by default they are hidden from `ls` (**l**ist content**s**). Let's enter the commands below to see what's available in the home directory of newuser.

```sh
newuser@25ec7419d9e7:~$ ls
newuser@25ec7419d9e7:~$ ls -a
.  ..  .bash_logout  .bashrc  .profile
newuser@25ec7419d9e7:~$ ls -al
total 20
drwxr-xr-x 2 newuser newuser 4096 Feb 13 19:45 .
drwxr-xr-x 1 root    root    4096 Feb 13 19:45 ..
-rw-r--r-- 1 newuser newuser  220 Apr  4  2018 .bash_logout
-rw-r--r-- 1 newuser newuser 3771 Apr  4  2018 .bashrc
-rw-r--r-- 1 newuser newuser  807 Apr  4  2018 .profile
```

Reviewing the above commands, we see that `ls` alone didn't show us the dotfiles. However, once we added the `-a` (**a**ll) flag, we then see the files. My favorite is to add the `-l` (**l**ong) flag, as you can see in the third iteration of that command because it tells us a lot more about the files, such as permissions, ownership, size, and creation date of the file (more on all of these later). 

The `.bashrc` and `.profile` files are important because they are read and processed right before newuser logs into his/her account. For that reason, this is where we put things we want to "autoload" when shell into this environment. Some of things could be modifying our `$PATH`, creating function, and creating some really handy things call aliases.

Before we dive into text editors like vim, let's learn how to view the contents of a file using the `cat` (con**cat**enate and printout files) command. Type the following into your terminal:

`cat .bashrc`

That was a lot of text, huh? One way we could slow that down is by using a command called `less.` `less` is nice because it doesn't load the whole file before showing it to you, so if you have really large files, it's nice to start browsing with `less`. Type the following into your terminal, and notice you can use the `↑` and `↓` arrows to navigate the file, but you can't edit it (press `q` to exit):

`less .bashrc`

## Commands & Concepts We Covered
 
  - `su` - Switch user.
  - `ls` - List contents of a directory.
  - `cd` - Change directory.
  - `$HOME == ~` - The `$HOME` envvar and `~` (tilde) are the same.
  - `/` - The "root" of the file system.
  - `Dotfiles` aren't visible to `ls` without the "all" (`-a`) flag passed into it.
  - `cat` can both print the contents of a file, and concatenate files.
  - `less` is another lightweight way to view files.
  - `.bashrc`, `.bash_profile`, and `.profile` are all common "startup" shell scripts that contain aliases, functions, and environment variables.