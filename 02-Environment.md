# Environment

> **Prereqs**
> - Usage of a terminal/command line interface
> - Docker installed
> - Usage of `linuxtut` Docker container

## Who and Where Am I?
When you first drop into a shell, you may wonder *where* you are and *who* are. Open your terminal program and type `whoami` to find out who you are and `pwd` (**p**rint **w**orking **d**irectory) to find out where. Then let's find out *which* system we're on and what distro we are using the `uname` command. Here's what mine looks like:

```sh
$ whoami
nsorrell
$ pwd
/home/nsorrell/src/LinuxTutorial
$ uname -a
Linux nsorrell-laptop 5.0.0-38-generic #41-Ubuntu SMP Tue Dec 3 00:27:35 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux
```

As you can see, the uname command tells me my hostname (nsorrell-laptop) in the second argument, and my distro (#41-Ubuntu) in the fourth. This will come in handy to make sure we are executing commands on the right system!

For the kind of stuff we're going to be doing, we won't want to accidentally screw up our real OS, so we will use a Docker container to explore shells and commands. Still inside of your terminal, type the following Docker command, which should then put you inside the container:

`docker run -it sorrell/linuxtut bash`

On my system it looks like this - note I'll run those commands we learned above to find who I just logged in as and where I am. Note, all of that is taking place *inside* of the shell that is *inside* the container.

```sh
$ docker run -it sorrell/linuxtut bash
root@74b2d3049008:/# whoami
root
root@74b2d3049008:/# pwd
/
root@74b2d3049008:/# uname -a
Linux 74b2d3049008 5.0.0-38-generic #41-Ubuntu SMP Tue Dec 3 00:27:35 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux
```

Lots of useful information up there! First, it tells us we are `root`, and that our `pwd` (working directory) is `/`, which is also called the root of the filesystem (we will get more into that later). Importantly this also tells us that we are on the `74b2d3049008` host in the `uname` response. Your hostname will be different, since this value is different for all Docker containers, and you will even see it change throughout this tutorial because I start and stop containers all the time. Since we are in the Docker container, it means we are safe to operate any command we want here without fear of screwing up our host OS. Containers are disposable (and ephemeral, more on that later), so no matter what we do to this container, we can just launch a new one.

That container hostname will also be visible if we inspect on our host (not inside of our container):

```sh
$ docker ps
CONTAINER ID        IMAGE                COMMAND                  CREATED             STATUS              
74b2d3049008        intxlog/apache       "docker-entrypoint.s…"   23 minutes ago      Up 23 minutes
```

## What else is in our environment?

We can find out more about our environment by inspecting which environment variables are set. We will do that using the `printenv` command.

```sh
root@74b2d3049008:/# printenv
LC_ALL=en_US.UTF-8
NVM_DIR=/root/.nvm
HOSTNAME=74b2d3049008
DOCKERIZE_VERSION=v0.6.1
NVM_CD_FLAGS=
PYTHONIOENCODING=UTF-8
TINI_VERSION=v0.18.0
NVM_VERSION=0.35.1
COMPOSER_ALLOW_SUPERUSER=1
PWD=/
HOME=/root
GOSU_VERSION=1.11
COMPOSER_ALLOW_XDEBUG=1
DEBIAN_FRONTEND=noninteractive
NODE_VERSION=10.18.1
COMPOSER_DISABLE_XDEBUG_WARN=1
TERM=xterm
SHELL=/bin/bash
NVM_BIN=/root/.nvm/versions/node/v10.18.1/bin
PHP_VERSION=7.3
SHLVL=1
LANGUAGE=en_US.UTF-8
PATH=/root/.nvm/versions/node/v10.18.1/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
COMPOSER_MEMORY_LIMIT=-1
_=/usr/bin/printenv
```

Lots of interesting variables up there! We can access then directly by putting a dollar sign in front them. For example, if we want to see just the `PHP_VERSION` we can type

```sh
root@74b2d3049008:/# echo $PHP_VERSION 
7.3
```

As you can see, the `echo` command does what we expect it to: echoes out the value of a variable. Let's examine some of the environment variables above:

  - `$PATH` - This is one of the most imporant environment variables. The "path" specifies default folders for the shell to look in when you type a command. So when you type `ls`, the shell searches all of the folders in the path looking for that command. Commands/executables that are not in the path need to be executed either inside of their folder or using their full path (more on this later).
    + What exactly to do we mean? Well, when you type `ls`, the shell finds `ls` in the `/bin` folder and executes that. You could also just type `/bin/ls` to execute the command also. So including that directory in your `PATH` saves you keystrokes!
  - `$_` - This is the variable for the script/command executed. As you can see above, it is our `printenv` command, and it contains the full path of that command. Note that the `/usr/bin` part is part of our `$PATH`!
  - `$SHELL` - This tells us our default shell type - bash for this container.
  - `$HOME` - This tells us where our home directory is for this user.

## Create your own environment variables

One handy thing to do is create your own environment variables (envvar). This can be helpful, as seen above for example, when you want every shell script to reference the same `PHP_VERSION`. So how do we set an envvar? It's pretty easy, as illustrated below (note, there can be __no space__ between the `=` and the name and the value when you assign this).

```sh
root@01001b56377d:/# MY_VAR="Hello world! This is my new envvar!"
root@01001b56377d:/# echo $MY_VAR
Hello world! This is my new envvar!
```

A couple of points here. First, variable assignment contains no `$` for the name, but we do the `$` to reference a variable.

And, we won't see this variable when we `printenv` - go ahead and try. Why is this and what does it mean? Well, we only created that variable for *this shell session*, so any new processes we run won't have access to it. Let's verify by starting *another* shell session within the one we are already in!

```sh
root@50f4cbf77e48:/# echo $$
1
root@50f4cbf77e48:/# bash
root@50f4cbf77e48:/# echo $$
16
root@50f4cbf77e48:/# echo $MY_VAR

root@50f4cbf77e48:/#
```

As you can see, we start another shell session by entering the `bash` command, but before that we echo `$$` which is the **p**rocess **ID** (also known as a PID) of the current shell session (1) - I did this so you can easily see that we actually enter into a *new* bash shell with the `bash` command with a PID of 16 (your PID may vary).  And then when we echo our envvar, and get nothing back. Let's `exit` back to our parent session and then `export` the variable so it's accessible to our future sessions.

```sh
root@50f4cbf77e48:/# MY_VAR="Hello world! This is my new envvar!"
root@50f4cbf77e48:/# echo $MY_VAR
Hello world! This is my new envvar!
root@50f4cbf77e48:/# echo $$
1
root@50f4cbf77e48:/# bash
root@50f4cbf77e48:/# echo $$
16
root@50f4cbf77e48:/# echo $MY_VAR

root@50f4cbf77e48:/# exit
exit
root@50f4cbf77e48:/# export MY_VAR
root@50f4cbf77e48:/# echo $$
1
root@50f4cbf77e48:/# bash
root@50f4cbf77e48:/# echo $$
22
root@50f4cbf77e48:/# echo $MY_VAR
Hello world! This is my new envvar!
```

You'll see the `export` commands in a lot of scripts, especially your shell .profile and .rc scripts, because you are usually configuring your shell sessions in these files (more on that in Dotfiles). 

## Commands & Concepts We Covered

  - `whoami` - Print current user name.
  - `pwd` - Print current (working) directory.
  - `uname` - Print system information.
  - `printenv` - Print all the environment variables currently available.
  - `echo` - Print variables/strings to the shell.
  - `export` - Used to export variables so that future shell sessions/processes can access the variable.
  - `exit` - Used to exit a shell session.
  - `PID` - Numeric ID of a running process.
  - The `PATH` variable - Indicates where the system will look for binaries by default.