# What is your PATH?

We briefly touched on aspects of the `PATH` in chapters [2](02-Environment.md), [3](03-Dotfiles.md), and [4](04-Aliases.md). Here's a quick recap of what we covered there:

  - (Chapter 2): `PATH` is one of the most imporant environment variables. The `PATH` specifies default folders for the shell to look in when you type a command. So when you type `ls`, the shell searches all of the folders in the `PATH` looking for that command. Commands/executables that are not in the `PATH` need to be executed either inside of their folder or using their full path.
  - (Chapter 3 & 4): `PATH` is typically defined in your `.bashrc`, `.profile`, or `.bash_profile`. We discovered where by using `grep`.

Before we dig further into the details of `PATH`, let's take a brief diversion into the difference between `PATH`, path, relative path, and absolute path.

## Many paths

`PATH` is an environment variable, and as we've talked about, helps your shell locate executables without specifying their **absolute path** (you might also hear people call this the **full path**). Think of an absolute path like your full mailing address. It **always starts at the root** and then specifies the subfolders/files from there. Let's take a look below:

```sh
newuser@50f4cbf77e48:~$ which ls
/bin/ls
```

So that full address, or absolute path says:

  1. Start at the root (`/`)
  2. Go to `bin`
  3. There you'll find `ls`

So then what is a relative path? Well, it is relative to your current working directory (remember the command `pwd` and the envvar `$PWD`?). So, let's `cd` home, then `ls` some directories relative to our current directory. The output from the commands is a bit lengthy, so I've shortened it below to just the commands. Every `ls` command below, unless otherwise noted, is given a *relative* path to search. 

```sh
$ cd ~ # go to our HOME
$ ls -la ..  # .. means our "parent" directory (/home)
$ ls -la ../.. # ../.. means our parent's parent directory (/)
$ ls -la ../../tmp # means our paren't parent's child directory (/tmp)
$ ls -la . # . means our current directory
$ cd / # go to the root of the filesystem
$ ls home/$USER # home is the child of /, so since we are in / we don't need to specify /home
$ ls /home/$USER # this is an ABSOLUTE path - see how we get the same results as above?
```