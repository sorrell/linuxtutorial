
# Aliases

> **Prereqs**
> - Usage of `linuxtut` Docker container
> - Usage of the user `newuser` inside container (`su` if needed)
> - Usage of `cat`
> - Knowledge of [dotfiles](03-Dotfiles.md)

Aliases are great timesavers - they are shortcuts for things we often do. Let's find out what aliases are in our `.bashrc` by using `cat`, but this time we're going to add a little extra to our command to get exactly what we want.

```sh
newuser@25ec7419d9e7:~$ cat .bashrc | grep alias
# enable color support of ls and also add handy aliases
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
# Add an "alert" alias for long running commands.  Use like so:
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'
# ~/.bash_aliases, instead of adding them here directly.
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
```

We did two things with that command.

  1. We "piped" the results from our `cat` command using the `|` symbol. This tells the subsequent command to use the results in its own command.
  2. We used `grep` (**g**et **re**gular ex**p**ression) to search for the word "alias" in the results of our first command that were piped into `grep`

And look at all of those aliases! One of my favorites is `ll`, which looks pretty familiar to our helpful command from before, `ls -al`! Let's try it and notice how we get the same results!

```sh
newuser@25ec7419d9e7:~$ ll
total 28
drwxr-xr-x 1 newuser newuser 4096 Feb 13 20:51 ./
drwxr-xr-x 1 root    root    4096 Feb 13 19:45 ../
-rw-r--r-- 1 newuser newuser  220 Apr  4  2018 .bash_logout
-rw-r--r-- 1 newuser newuser 3771 Apr  4  2018 .bashrc
-rw-r--r-- 1 newuser newuser  807 Apr  4  2018 .profile
```

Using `cat` was nice, but it was unneccesary with `grep`, notice below how we can acheive the same results by specifying the filename (.bashrc) and just using `grep`.

```sh
newuser@25ec7419d9e7:~$ grep alias .bashrc
# enable color support of ls and also add handy aliases
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
# Add an "alert" alias for long running commands.  Use like so:
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'
# ~/.bash_aliases, instead of adding them here directly.
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
```

But remember `cat` can concatenate files? Maybe we could search two files for the word "path" using `cat` - let's try!

```sh
newuser@25ec7419d9e7:~$ cat .bashrc .profile | grep -i path
# If set, the pattern "**" used in a pathname expansion context will
# set PATH so it includes user's private bin if it exists
    PATH="$HOME/bin:$PATH"
# set PATH so it includes user's private bin if it exists
    PATH="$HOME/.local/bin:$PATH"
```

That's interesting, but we can't see which "path" match came from which file... let's try `grep` alone again to help:

```sh
newuser@25ec7419d9e7:~$ grep -ir path
.bashrc:# If set, the pattern "**" used in a pathname expansion context will
.profile:# set PATH so it includes user's private bin if it exists
.profile:    PATH="$HOME/bin:$PATH"
.profile:# set PATH so it includes user's private bin if it exists
.profile:    PATH="$HOME/.local/bin:$PATH"
```

Cool, that helped! Now we can see which file is setting our `PATH` variable, and which one just mentions it. I used a couple of flags to help me in my search:

  1. `-i` - case insensitive search
  2. `-r` - recursively go through files and directories starting at the current directory


## Commands & Concepts We Covered
 
  - PIPES
  - TODO