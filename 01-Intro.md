# Linux Tutorial

## Prerequisites
For this course you will need:

 1. Use a *nix based system. This can be a Linux distro, MacOS (using Darwin based terminal), or Windows with WSL2.
 2. Docker installed. We will use containers to learn inside of.

## History, Distributions (Distros), Desktop/Server
Rather than get into the minutae here, the brief version is that Unix is an OS that has origins from the early 1970's at AT&T Bell Labs, and that spawned many offshoots/variants. The modern Linux/Darwin core has roots in Unix, but is not "Unix" - in the same way that React has roots in Javascript but is not "pure" Javascript.

That said, if you're interested in more of history, [see here](https://dwheeler.com/secure-programs/Secure-Programs-HOWTO/history.html). 

In terms of "distros," these are the "products" built on top of the core OS. Darwin OS, in fact, has many Apple distros, including MacOS, iOS, tvOS and more. Linux OS has *many* more distros. Some of the more popular are Ubuntu, Debian, Fedora, Red Hat, Mint, CentOS, Arch, and elementary. And, there are distros built from other distros! Manjaro provides a GUI on top of Arch, XUbuntu swaps out a lighter GUI for the one in Ubuntu, and Red Hat is a commercial "snapshot" of Fedora with extra security and testing and apps.

Many of these distros come in both desktop and server versions. The desktop versions have a GUI that you can use to open programs like web browsers, text editors, etc. The server versions typically don't have a GUI, and are operated through a shell (command line). 

All of the Linux distros also have a "package manager" which makes it easy to install (typically trusted) software. In Ubuntu there is `apt`, in Fedora there is `yum`, and Arch has `pacman`. On MacOS, you can install `brew` to help with this need. The packages that the package managers can install are binary files that only that manager can run. For example, `yum` on Fedora can't install packages built for `apt`.  Ubuntu packages end with the extension `.deb` (Debian) and Fedora packages end with the `.rpm` extension. There are also many emerging cross-distro package managers, like AppImage, Snappy, and Flatpak.

## Shells, Terminals, Consoles, Command Line
These are all fairly synonymous, but for a better history, [see here](https://www.hanselman.com/blog/WhatsTheDifferenceBetweenAConsoleATerminalAndAShell.aspx). The short version is this: the terminal is the "graphical" program that runs a shell. VSCode has its own Terminal, but that terminal is really just running a shell. Think of it like your cell phone, where it's the phone screen (terminal) that shows you the app (shell) you're running.

There are many different kinds of shells, with the following being the most popular.

  - `bash` - the "**B**ourne **A**gain **SH**ell". This is the default shell on a lot of different platforms, including MacOS, Ubuntu, and Fedora.
  - `zsh` - this has become a developer favorite because of popular mods like Oh-my-zsh and Prezto.
  - `dash` - the "native" shell in many Docker containers.  
  - `sh` - this is typically just an "alias" that points to bash or zsh or another shell
  - `ssh` - **S**ecure **SH**ell actually isn't even a shell itself, but rather a secure connection between two shells. For example, you could be running `zsh` and run the `ssh` command to connect to an Ubuntu server whose default is `bash`. Your terminal would then be displaying the server `bash` shell, via the encrypted tunnel that ssh provided in your `zsh` shell!

For this tutorial, we will be using `bash`, but most of this tutorial is interoperable with other shells. For example, if you're using `zsh`, realize there are `.zhistory`, `.zshrc` and other files that are counterparts to their `bash` equivalents.

Shells are called "shells" because they actually don't do much themselves, but are rather shells around the rest of the possible commands. For example, whether you use `zsh` or `bash`, when you run the `ls` command to look at the contents of a directory, you are calling the same `ls` program regardless of which shell you are using! Shells provide the platform to run all of the "command line" commands you have ever run, but those shells didn't install or "own" those commands (which are actually programs).

As with all great inventions, people always want more of a good thing. So how do we create multiple terminals in a single screen? That's what programs like `tmux` and `screen` are for. They aren't "shells", but rather they are called "multiplexers." They provide multiple "views" inside of a single shell. You might wonder why we would do this. Well this can be very helpful when we need to see two things happening at once, such as following logs. Below is a view of a `tmux` session where I am following two different docker container logs. 

![tmux](assets/tmux.png)